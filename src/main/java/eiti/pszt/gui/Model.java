package eiti.pszt.gui;

import eiti.pszt.model.Path;
import eiti.pszt.model.Point;

import java.util.List;

public class Model {

    private final Point source;
    private final Point goal;

    private final List<Path> paths;

    private int pathIndex;

    public Model(List<Path> paths, Point source, Point goal) {
        this.paths = paths;
        this.source = source;
        this.goal = goal;
    }

    public Point getSource() {
        return source;
    }

    public Point getGoal() {
        return goal;
    }

    public void setIterationToShow(int index) {
        this.pathIndex = index;
    }

    public Path getPath() {
        return paths.get(pathIndex);
    }

    public int getLastPathIndex() {
        return paths.size() - 1;
    }
}
