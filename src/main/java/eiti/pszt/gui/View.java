package eiti.pszt.gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

public class View {

    private final JPanel pathPanel;
    private final Model model;

    public View(Model model) {
        this.model = model;
        pathPanel = new PathPanel(model);
        JFrame frame = new JFrame();
        frame.add(createSlider(), BorderLayout.SOUTH);
        frame.add(pathPanel);
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }

    private JSlider createSlider() {
        JSlider slider = new JSlider();
        slider.setValue(0);
        slider.setMinimum(0);
        slider.setMaximum(model.getLastPathIndex());
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);
        slider.setMajorTickSpacing((model.getLastPathIndex() + 1) / 10);
        slider.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                JSlider source = (JSlider) e.getSource();
                int value = source.getValue();
                model.setIterationToShow(value);
                redraw();
            }
        });
        return slider;
    }

    private void redraw() {
        SwingUtilities.invokeLater(pathPanel::repaint);
    }
}
