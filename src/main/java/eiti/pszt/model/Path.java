package eiti.pszt.model;

import java.util.Iterator;
import java.util.List;

public class Path {

    private final List<Point> points;

    public Path(List<Point> points) {
        this.points = points;
    }

    public List<Point> getPoints() {
        return points;
    }

    public int segmentCount() {
        return points.size();
    }

    public double length() {
        double total = 0;
        Iterator<Point> iterator = points.iterator();
        Point first = iterator.next();
        while (iterator.hasNext()) {
            Point second = iterator.next();
            total += first.distanceTo(second);
            first = second;
        }
        return total;
    }

    @Override
    public String toString() {
        return points.toString();
    }
}
