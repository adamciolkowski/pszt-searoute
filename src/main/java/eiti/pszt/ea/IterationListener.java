package eiti.pszt.ea;

import eiti.pszt.model.Path;

public interface IterationListener {

    void onIterationEnd(Path bestSoFar);
}
