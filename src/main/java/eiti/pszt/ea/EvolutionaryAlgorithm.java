package eiti.pszt.ea;

import eiti.pszt.model.Path;
import eiti.pszt.model.Point;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import static eiti.pszt.utils.Utils.concat;
import static eiti.pszt.utils.Utils.randomInRange;
import static java.lang.Math.exp;
import static java.lang.Math.sqrt;
import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.toList;

public class EvolutionaryAlgorithm {

    private final int mi;
    private final int lambda;

    private static final Random random = new Random();
    private static final Comparator<Path> comparingLength = comparingDouble(Path::length);
    private final Point source;
    private final Point goal;

    private IterationListener iterationListener = bestSoFar -> { };

    public EvolutionaryAlgorithm(int mi, int lambda, Point source, Point goal) {
        this.mi = mi;
        this.lambda = lambda;
        this.source = source;
        this.goal = goal;
    }

    public Path run(int iterations) {
        List<Path> population = initPopulation(mi);
        for (int i = 0; i < iterations; i++) {
            population = doIteration(population);
            Path bestSoFar = Collections.min(population, comparingLength);
            iterationListener.onIterationEnd(bestSoFar);
        }
        return Collections.min(population, comparingLength);
    }

    private List<Path> initPopulation(int size) {
        List<Path> parents = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            parents.add(randomPath());
        }
        return parents;
    }

    private List<Path> doIteration(List<Path> population) {
        List<Path> children = chooseBestChildrenFrom(population);
        population.addAll(children);
        population = population.stream()
                .sorted(comparingLength)
                .limit(mi)
                .collect(toList());

        population = crossover(population);
        mutate(population);
        return population;
    }

    private List<Path> chooseBestChildrenFrom(List<Path> parents) {
        return parents.stream()
                .sorted(comparingDouble(Path::length))
                .limit(lambda)
                .collect(toList());
    }

    private List<Path> crossover(List<Path> population) {
        List<Path> cross = new ArrayList<>();
        for (int j = 0; j < population.size() - 1; j++) {
            cross.add(crossover(population.get(j), population.get(j + 1)));
        }
        cross.add(crossover(population.get(population.size() - 1), population.get(0)));
        return cross;
    }

    private Path crossover(Path path1, Path path2) {
        int c = path1.segmentCount();
        int pivot = randomInRange(1, c - 1, random);
        List<Point> points1 = path1.getPoints();
        List<Point> points2 = path2.getPoints();
        List<Point> points = concat(points1.subList(0, pivot), points2.subList(pivot, c));
        return new Path(points);
    }

    private void mutate(List<Path> population) {
        for (Path path : population) {
            mutatePath(path);
        }
    }

    private void mutatePath(Path path) {
        List<Point> points = path.getPoints();
        for (int i = 1; i < points.size() - 1; i++) {
            Point point = points.get(i);
            double factor = factorFor(i);
            double dx = factor * random.nextGaussian();
            double dy = factor * random.nextGaussian();
            points.set(i, point.translate(dx, dy));
        }
    }

    private double factorFor(int i) {
        double tau = 1 / sqrt(2.0 * sqrt(i));
        double tauPrime = 1 / sqrt(2.0 * i);
        return random.nextGaussian() * exp(tauPrime * random.nextGaussian() +
                tau * random.nextGaussian());
    }

    private Path randomPath() {
        List<Point> points = new ArrayList<>();
        points.add(source);
        for (int i = 0; i < 8; i++) {
            double x = randomInRange(source.getX(), goal.getX(), random);
            double y = randomInRange(source.getY(), goal.getY(), random);
            points.add(Point.of(x, y));
        }
        points.add(goal);
        return new Path(points);
    }

    public void setIterationListener(IterationListener l) {
        this.iterationListener = l;
    }
}
