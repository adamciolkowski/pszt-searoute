package eiti.pszt;

import eiti.pszt.ea.EvolutionaryAlgorithm;
import eiti.pszt.gui.Model;
import eiti.pszt.gui.View;
import eiti.pszt.model.Path;
import eiti.pszt.model.Point;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    private void run() {
        Point source = Point.ZERO;
        Point goal = Point.of(800, 500);
        List<Path> paths = getPaths(source, goal);
        Model model = new Model(paths, source, goal);
        new View(model);
    }

    private List<Path> getPaths(Point source, Point goal) {
        EvolutionaryAlgorithm ea = new EvolutionaryAlgorithm(40, 5, source, goal);
        List<Path> paths = new ArrayList<>();
        ea.setIterationListener(paths::add);
        ea.run(600);
        return paths;
    }
}
